var imageCount = 1;
var imageTotal = 7;

function buttonSlide(x) {
	
	var Image = document.getElementById ("homeImage");
	
	imageCount = imageCount + x;	
	
	if (imageCount > imageTotal) {
		imageCount = 1;
	}
	if (imageCount < 1) {
		imageCount = imageTotal;
	}
	
	Image.src = "./images/slideshow/slideshow" + imageCount + ".jpg";
	
}


window.setInterval (function slideshow () {
	
	var banner = document.getElementById ("homeImage");
	
	imageCount++;
	
	if (imageCount > imageTotal) {
		imageCount = 1;
	}
	if (imageCount < 1) {
		imageCount = imageTotal;
	}
	
	banner.src = "./images/slideshow/slideshow" + imageCount + ".jpg";
	
}, 5000);